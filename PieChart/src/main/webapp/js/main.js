var startDate = null;
var endDate = null;


$(document).ready(function () {
        $("#txtFromDate").datepicker({
            dateFormat:'dd-mm-yy',
            numberOfMonths: 1,
            onSelect: function (selected) {
                $("#txtToDate").datepicker("option", "minDate", selected)
                startDate = $( "#txtFromDate" ).datepicker({ dateFormat: 'dd-mm-yy' }).val();
            }
        });

        $("#txtToDate").datepicker({
            dateFormat:'dd-mm-yy',
            numberOfMonths: 1,
            onSelect: function (selected) {
                $("#txtFromDate").datepicker("option", "maxDate", selected)
               endDate = $( "#txtToDate" ).datepicker({ dateFormat: 'dd-mm-yy' }).val();
            }
        });
    });

    $(document).ready(function () {
        $("button").click(


            function () {
               var element = document.getElementById('example-percent-container');

                var seconds = new ProgressBar.Circle(element, {
                    duration: 200,
                    color: "#FCB03C",
                    trailColor: "#ddd"
                });

                setInterval(function() {
                    var second = new Date().getSeconds();
                    seconds.animate(second / 60, function() {
                        seconds.setText(second);
                    });
                }, 1000)

                $.ajax(
                    {
                        url: '/rest/getLastFiveMinutes',
                        type: 'POST',
                        timeout: 1000 * 60 * 60 * 60,
                        data://JSON.stringify(
                        {
                            startDate: $( "#txtFromDate" ).datepicker({ dateFormat: 'dd-mm-yy' }).val(),
                            endDate:$( "#txtToDate" ).datepicker({ dateFormat: 'dd-mm-yy' }).val()
                        },

                        complete:
                        function(response, status){
                            combinationChart($.parseJSON( response.responseText) );
                            //$("#container").css("display","block");
                            $("#container").show();
                            $("#example-percent-container").hide();
                          //  document.getElementsByTagName("container")


                        }
                    }
                );



            }
        );
    });






