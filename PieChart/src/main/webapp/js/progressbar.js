

(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.ProgressBar = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
    /*! shifty - v1.2.2 - 2014-10-09 - http://jeremyckahn.github.io/shifty */
    ;(function (root) {




        if (typeof SHIFTY_DEBUG_NOW === 'undefined') {
            SHIFTY_DEBUG_NOW = function () {
                return +new Date();
            };
        }

        var Tweenable = (function () {

            'use strict';

            // Aliases that get defined later in this function
            var formula;

            // CONSTANTS
            var DEFAULT_SCHEDULE_FUNCTION;
            var DEFAULT_EASING = 'linear';
            var DEFAULT_DURATION = 500;
            var UPDATE_TIME = 1000 / 60;

            var _now = Date.now
                ? Date.now
                : function () {return +new Date();};

            var now = SHIFTY_DEBUG_NOW
                ? SHIFTY_DEBUG_NOW
                : _now;

            if (typeof window !== 'undefined') {

                DEFAULT_SCHEDULE_FUNCTION = window.requestAnimationFrame
                    || window.webkitRequestAnimationFrame
                    || window.oRequestAnimationFrame
                    || window.msRequestAnimationFrame
                    || (window.mozCancelRequestAnimationFrame
                    && window.mozRequestAnimationFrame)
                    || setTimeout;
            } else {
                DEFAULT_SCHEDULE_FUNCTION = setTimeout;
            }

            function noop () {
                // NOOP!
            }


            function each (obj, fn) {
                var key;
                for (key in obj) {
                    if (Object.hasOwnProperty.call(obj, key)) {
                        fn(key);
                    }
                }
            }


            function shallowCopy (targetObj, srcObj) {
                each(srcObj, function (prop) {
                    targetObj[prop] = srcObj[prop];
                });

                return targetObj;
            }


            function defaults (target, src) {
                each(src, function (prop) {
                    if (typeof target[prop] === 'undefined') {
                        target[prop] = src[prop];
                    }
                });
            }


            function tweenProps (forPosition, currentState, originalState, targetState,
                                 duration, timestamp, easing) {
                var normalizedPosition = (forPosition - timestamp) / duration;

                var prop;
                for (prop in currentState) {
                    if (currentState.hasOwnProperty(prop)) {
                        currentState[prop] = tweenProp(originalState[prop],
                            targetState[prop], formula[easing[prop]], normalizedPosition);
                    }
                }

                return currentState;
            }


            function tweenProp (start, end, easingFunc, position) {
                return start + (end - start) * easingFunc(position);
            }



            function applyFilter (tweenable, filterName) {
                var filters = Tweenable.prototype.filter;
                var args = tweenable._filterArgs;

                each(filters, function (name) {
                    if (typeof filters[name][filterName] !== 'undefined') {
                        filters[name][filterName].apply(tweenable, args);
                    }
                });
            }

            var timeoutHandler_endTime;
            var timeoutHandler_currentTime;
            var timeoutHandler_isEnded;

            function timeoutHandler (tweenable, timestamp, duration, currentState,
                                     originalState, targetState, easing, step, schedule) {
                timeoutHandler_endTime = timestamp + duration;
                timeoutHandler_currentTime = Math.min(now(), timeoutHandler_endTime);
                timeoutHandler_isEnded = timeoutHandler_currentTime >= timeoutHandler_endTime;

                if (tweenable.isPlaying() && !timeoutHandler_isEnded) {
                    schedule(tweenable._timeoutHandler, UPDATE_TIME);

                    applyFilter(tweenable, 'beforeTween');
                    tweenProps(timeoutHandler_currentTime, currentState, originalState,
                        targetState, duration, timestamp, easing);
                    applyFilter(tweenable, 'afterTween');

                    step(currentState);
                } else if (timeoutHandler_isEnded) {
                    step(targetState);
                    tweenable.stop(true);
                }
            }



            function composeEasingObject (fromTweenParams, easing) {
                var composedEasing = {};

                if (typeof easing === 'string') {
                    each(fromTweenParams, function (prop) {
                        composedEasing[prop] = easing;
                    });
                } else {
                    each(fromTweenParams, function (prop) {
                        if (!composedEasing[prop]) {
                            composedEasing[prop] = easing[prop] || DEFAULT_EASING;
                        }
                    });
                }

                return composedEasing;
            }


            function Tweenable (opt_initialState, opt_config) {
                this._currentState = opt_initialState || {};
                this._configured = false;
                this._scheduleFunction = DEFAULT_SCHEDULE_FUNCTION;


                if (typeof opt_config !== 'undefined') {
                    this.setConfig(opt_config);
                }
            }


            Tweenable.prototype.tween = function (opt_config) {
                if (this._isTweening) {
                    return this;
                }


                if (opt_config !== undefined || !this._configured) {
                    this.setConfig(opt_config);
                }

                this._start(this.get());
                return this.resume();
            };



            Tweenable.prototype.setConfig = function (config) {
                config = config || {};
                this._configured = true;

                // Init the internal state
                this._pausedAtTime = null;
                this._start = config.start || noop;
                this._step = config.step || noop;
                this._finish = config.finish || noop;
                this._duration = config.duration || DEFAULT_DURATION;
                this._currentState = config.from || this.get();
                this._originalState = this.get();
                this._targetState = config.to || this.get();
                this._timestamp = now();

                // Aliases used below
                var currentState = this._currentState;
                var targetState = this._targetState;

                // Ensure that there is always something to tween to.
                defaults(targetState, currentState);

                this._easing = composeEasingObject(
                    currentState, config.easing || DEFAULT_EASING);

                this._filterArgs =
                    [currentState, this._originalState, targetState, this._easing];

                applyFilter(this, 'tweenCreated');
                return this;
            };


            Tweenable.prototype.get = function () {
                return shallowCopy({}, this._currentState);
            };


            Tweenable.prototype.set = function (state) {
                this._currentState = state;
            };


            Tweenable.prototype.pause = function () {
                this._pausedAtTime = now();
                this._isPaused = true;
                return this;
            };


            Tweenable.prototype.resume = function () {
                if (this._isPaused) {
                    this._timestamp += now() - this._pausedAtTime;
                }

                this._isPaused = false;
                this._isTweening = true;

                var self = this;
                this._timeoutHandler = function () {
                    timeoutHandler(self, self._timestamp, self._duration, self._currentState,
                        self._originalState, self._targetState, self._easing, self._step,
                        self._scheduleFunction);
                };

                this._timeoutHandler();

                return this;
            };


            Tweenable.prototype.stop = function (gotoEnd) {
                this._isTweening = false;
                this._isPaused = false;
                this._timeoutHandler = noop;

                if (gotoEnd) {
                    shallowCopy(this._currentState, this._targetState);
                    applyFilter(this, 'afterTweenEnd');
                    this._finish.call(this, this._currentState);
                }

                return this;
            };

            Tweenable.prototype.isPlaying = function () {
                return this._isTweening && !this._isPaused;
            };


            Tweenable.prototype.setScheduleFunction = function (scheduleFunction) {
                this._scheduleFunction = scheduleFunction;
            };


            Tweenable.prototype.dispose = function () {
                var prop;
                for (prop in this) {
                    if (this.hasOwnProperty(prop)) {
                        delete this[prop];
                    }
                }
            };


            Tweenable.prototype.filter = {};


            Tweenable.prototype.formula = {
                linear: function (pos) {
                    return pos;
                }
            };

            formula = Tweenable.prototype.formula;

            shallowCopy(Tweenable, {
                'now': now
                ,'each': each
                ,'tweenProps': tweenProps
                ,'tweenProp': tweenProp
                ,'applyFilter': applyFilter
                ,'shallowCopy': shallowCopy
                ,'defaults': defaults
                ,'composeEasingObject': composeEasingObject
            });


            if (typeof SHIFTY_DEBUG_NOW === 'function') {
                root.timeoutHandler = timeoutHandler;
            }


            if (typeof exports === 'object') {
                // CommonJS
                module.exports = Tweenable;
            } else if (typeof define === 'function' && define.amd) {
                // AMD
                define(function () {return Tweenable;});
            } else if (typeof root.Tweenable === 'undefined') {
                // Browser: Make `Tweenable` globally accessible.
                root.Tweenable = Tweenable;
            }

            return Tweenable;

        } ());




        ;(function () {

            Tweenable.shallowCopy(Tweenable.prototype.formula, {
                easeInQuad: function (pos) {
                    return Math.pow(pos, 2);
                },

                easeOutQuad: function (pos) {
                    return -(Math.pow((pos - 1), 2) - 1);
                },

                easeInOutQuad: function (pos) {
                    if ((pos /= 0.5) < 1) {return 0.5 * Math.pow(pos,2);}
                    return -0.5 * ((pos -= 2) * pos - 2);
                },

                easeInCubic: function (pos) {
                    return Math.pow(pos, 3);
                },

                easeOutCubic: function (pos) {
                    return (Math.pow((pos - 1), 3) + 1);
                },

                easeInOutCubic: function (pos) {
                    if ((pos /= 0.5) < 1) {return 0.5 * Math.pow(pos,3);}
                    return 0.5 * (Math.pow((pos - 2),3) + 2);
                },

                easeInQuart: function (pos) {
                    return Math.pow(pos, 4);
                },

                easeOutQuart: function (pos) {
                    return -(Math.pow((pos - 1), 4) - 1);
                },

                easeInOutQuart: function (pos) {
                    if ((pos /= 0.5) < 1) {return 0.5 * Math.pow(pos,4);}
                    return -0.5 * ((pos -= 2) * Math.pow(pos,3) - 2);
                },

                easeInQuint: function (pos) {
                    return Math.pow(pos, 5);
                },

                easeOutQuint: function (pos) {
                    return (Math.pow((pos - 1), 5) + 1);
                },

                easeInOutQuint: function (pos) {
                    if ((pos /= 0.5) < 1) {return 0.5 * Math.pow(pos,5);}
                    return 0.5 * (Math.pow((pos - 2),5) + 2);
                },

                easeInSine: function (pos) {
                    return -Math.cos(pos * (Math.PI / 2)) + 1;
                },

                easeOutSine: function (pos) {
                    return Math.sin(pos * (Math.PI / 2));
                },

                easeInOutSine: function (pos) {
                    return (-0.5 * (Math.cos(Math.PI * pos) - 1));
                },

                easeInExpo: function (pos) {
                    return (pos === 0) ? 0 : Math.pow(2, 10 * (pos - 1));
                },

                easeOutExpo: function (pos) {
                    return (pos === 1) ? 1 : -Math.pow(2, -10 * pos) + 1;
                },

                easeInOutExpo: function (pos) {
                    if (pos === 0) {return 0;}
                    if (pos === 1) {return 1;}
                    if ((pos /= 0.5) < 1) {return 0.5 * Math.pow(2,10 * (pos - 1));}
                    return 0.5 * (-Math.pow(2, -10 * --pos) + 2);
                },

                easeInCirc: function (pos) {
                    return -(Math.sqrt(1 - (pos * pos)) - 1);
                },

                easeOutCirc: function (pos) {
                    return Math.sqrt(1 - Math.pow((pos - 1), 2));
                },

                easeInOutCirc: function (pos) {
                    if ((pos /= 0.5) < 1) {return -0.5 * (Math.sqrt(1 - pos * pos) - 1);}
                    return 0.5 * (Math.sqrt(1 - (pos -= 2) * pos) + 1);
                },

                easeOutBounce: function (pos) {
                    if ((pos) < (1 / 2.75)) {
                        return (7.5625 * pos * pos);
                    } else if (pos < (2 / 2.75)) {
                        return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
                    } else if (pos < (2.5 / 2.75)) {
                        return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
                    } else {
                        return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
                    }
                },

                easeInBack: function (pos) {
                    var s = 1.70158;
                    return (pos) * pos * ((s + 1) * pos - s);
                },

                easeOutBack: function (pos) {
                    var s = 1.70158;
                    return (pos = pos - 1) * pos * ((s + 1) * pos + s) + 1;
                },

                easeInOutBack: function (pos) {
                    var s = 1.70158;
                    if ((pos /= 0.5) < 1) {return 0.5 * (pos * pos * (((s *= (1.525)) + 1) * pos - s));}
                    return 0.5 * ((pos -= 2) * pos * (((s *= (1.525)) + 1) * pos + s) + 2);
                },

                elastic: function (pos) {
                    return -1 * Math.pow(4,-8 * pos) * Math.sin((pos * 6 - 1) * (2 * Math.PI) / 2) + 1;
                },

                swingFromTo: function (pos) {
                    var s = 1.70158;
                    return ((pos /= 0.5) < 1) ? 0.5 * (pos * pos * (((s *= (1.525)) + 1) * pos - s)) :
                    0.5 * ((pos -= 2) * pos * (((s *= (1.525)) + 1) * pos + s) + 2);
                },

                swingFrom: function (pos) {
                    var s = 1.70158;
                    return pos * pos * ((s + 1) * pos - s);
                },

                swingTo: function (pos) {
                    var s = 1.70158;
                    return (pos -= 1) * pos * ((s + 1) * pos + s) + 1;
                },

                bounce: function (pos) {
                    if (pos < (1 / 2.75)) {
                        return (7.5625 * pos * pos);
                    } else if (pos < (2 / 2.75)) {
                        return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
                    } else if (pos < (2.5 / 2.75)) {
                        return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
                    } else {
                        return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
                    }
                },

                bouncePast: function (pos) {
                    if (pos < (1 / 2.75)) {
                        return (7.5625 * pos * pos);
                    } else if (pos < (2 / 2.75)) {
                        return 2 - (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
                    } else if (pos < (2.5 / 2.75)) {
                        return 2 - (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
                    } else {
                        return 2 - (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
                    }
                },

                easeFromTo: function (pos) {
                    if ((pos /= 0.5) < 1) {return 0.5 * Math.pow(pos,4);}
                    return -0.5 * ((pos -= 2) * Math.pow(pos,3) - 2);
                },

                easeFrom: function (pos) {
                    return Math.pow(pos,4);
                },

                easeTo: function (pos) {
                    return Math.pow(pos,0.25);
                }
            });

        }());


        ;(function () {
            // port of webkit cubic bezier handling by http://www.netzgesta.de/dev/
            function cubicBezierAtTime(t,p1x,p1y,p2x,p2y,duration) {
                var ax = 0,bx = 0,cx = 0,ay = 0,by = 0,cy = 0;
                function sampleCurveX(t) {return ((ax * t + bx) * t + cx) * t;}
                function sampleCurveY(t) {return ((ay * t + by) * t + cy) * t;}
                function sampleCurveDerivativeX(t) {return (3.0 * ax * t + 2.0 * bx) * t + cx;}
                function solveEpsilon(duration) {return 1.0 / (200.0 * duration);}
                function solve(x,epsilon) {return sampleCurveY(solveCurveX(x,epsilon));}
                function fabs(n) {if (n >= 0) {return n;}else {return 0 - n;}}
                function solveCurveX(x,epsilon) {
                    var t0,t1,t2,x2,d2,i;
                    for (t2 = x, i = 0; i < 8; i++) {x2 = sampleCurveX(t2) - x; if (fabs(x2) < epsilon) {return t2;} d2 = sampleCurveDerivativeX(t2); if (fabs(d2) < 1e-6) {break;} t2 = t2 - x2 / d2;}
                    t0 = 0.0; t1 = 1.0; t2 = x; if (t2 < t0) {return t0;} if (t2 > t1) {return t1;}
                    while (t0 < t1) {x2 = sampleCurveX(t2); if (fabs(x2 - x) < epsilon) {return t2;} if (x > x2) {t0 = t2;}else {t1 = t2;} t2 = (t1 - t0) * 0.5 + t0;}
                    return t2; // Failure.
                }
                cx = 3.0 * p1x; bx = 3.0 * (p2x - p1x) - cx; ax = 1.0 - cx - bx; cy = 3.0 * p1y; by = 3.0 * (p2y - p1y) - cy; ay = 1.0 - cy - by;
                return solve(t, solveEpsilon(duration));
            }

            function getCubicBezierTransition (x1, y1, x2, y2) {
                return function (pos) {
                    return cubicBezierAtTime(pos,x1,y1,x2,y2,1);
                };
            }
            // End ported code


            Tweenable.setBezierFunction = function (name, x1, y1, x2, y2) {
                var cubicBezierTransition = getCubicBezierTransition(x1, y1, x2, y2);
                cubicBezierTransition.x1 = x1;
                cubicBezierTransition.y1 = y1;
                cubicBezierTransition.x2 = x2;
                cubicBezierTransition.y2 = y2;

                return Tweenable.prototype.formula[name] = cubicBezierTransition;
            };


            Tweenable.unsetBezierFunction = function (name) {
                delete Tweenable.prototype.formula[name];
            };

        })();

        ;(function () {

            function getInterpolatedValues (
                from, current, targetState, position, easing) {
                return Tweenable.tweenProps(
                    position, current, from, targetState, 1, 0, easing);
            }

            // Fake a Tweenable and patch some internals.  This approach allows us to
            // skip uneccessary processing and object recreation, cutting down on garbage
            // collection pauses.
            var mockTweenable = new Tweenable();
            mockTweenable._filterArgs = [];


            Tweenable.interpolate = function (from, targetState, position, easing) {
                var current = Tweenable.shallowCopy({}, from);
                var easingObject = Tweenable.composeEasingObject(
                    from, easing || 'linear');

                mockTweenable.set({});

                // Alias and reuse the _filterArgs array instead of recreating it.
                var filterArgs = mockTweenable._filterArgs;
                filterArgs.length = 0;
                filterArgs[0] = current;
                filterArgs[1] = from;
                filterArgs[2] = targetState;
                filterArgs[3] = easingObject;

                // Any defined value transformation must be applied
                Tweenable.applyFilter(mockTweenable, 'tweenCreated');
                Tweenable.applyFilter(mockTweenable, 'beforeTween');

                var interpolatedValues = getInterpolatedValues(
                    from, current, targetState, position, easingObject);

                // Transform values back into their original format
                Tweenable.applyFilter(mockTweenable, 'afterTween');

                return interpolatedValues;
            };

        }());


        function token () {
            // Functionality for this extension runs implicitly if it is loaded.
        } /*!*/

// token function is defined above only so that dox-foundation sees it as
// documentation and renders it.  It is never used, and is optimized away at
// build time.

        ;(function (Tweenable) {

            /*!
             * @typedef {{
             *   formatString: string
             *   chunkNames: Array.<string>
             * }}
             */
            var formatManifest;

            // CONSTANTS

            var R_NUMBER_COMPONENT = /(\d|\-|\.)/;
            var R_FORMAT_CHUNKS = /([^\-0-9\.]+)/g;
            var R_UNFORMATTED_VALUES = /[0-9.\-]+/g;
            var R_RGB = new RegExp(
                'rgb\\(' + R_UNFORMATTED_VALUES.source +
                (/,\s*/.source) + R_UNFORMATTED_VALUES.source +
                (/,\s*/.source) + R_UNFORMATTED_VALUES.source + '\\)', 'g');
            var R_RGB_PREFIX = /^.*\(/;
            var R_HEX = /#([0-9]|[a-f]){3,6}/gi;
            var VALUE_PLACEHOLDER = 'VAL';

            // HELPERS

            var getFormatChunksFrom_accumulator = [];
            /*!
             * @param {Array.number} rawValues
             * @param {string} prefix
             *
             * @return {Array.<string>}
             */
            function getFormatChunksFrom (rawValues, prefix) {
                getFormatChunksFrom_accumulator.length = 0;

                var rawValuesLength = rawValues.length;
                var i;

                for (i = 0; i < rawValuesLength; i++) {
                    getFormatChunksFrom_accumulator.push('_' + prefix + '_' + i);
                }

                return getFormatChunksFrom_accumulator;
            }

            /*!
             * @param {string} formattedString
             *
             * @return {string}
             */
            function getFormatStringFrom (formattedString) {
                var chunks = formattedString.match(R_FORMAT_CHUNKS);

                if (!chunks) {
                    // chunks will be null if there were no tokens to parse in
                    // formattedString (for example, if formattedString is '2').  Coerce
                    // chunks to be useful here.
                    chunks = ['', ''];

                    // If there is only one chunk, assume that the string is a number
                    // followed by a token...
                    // NOTE: This may be an unwise assumption.
                } else if (chunks.length === 1 ||
                        // ...or if the string starts with a number component (".", "-", or a
                        // digit)...
                    formattedString[0].match(R_NUMBER_COMPONENT)) {
                    // ...prepend an empty string here to make sure that the formatted number
                    // is properly replaced by VALUE_PLACEHOLDER
                    chunks.unshift('');
                }

                return chunks.join(VALUE_PLACEHOLDER);
            }

            /*!
             * Convert all hex color values within a string to an rgb string.
             *
             * @param {Object} stateObject
             *
             * @return {Object} The modified obj
             */
            function sanitizeObjectForHexProps (stateObject) {
                Tweenable.each(stateObject, function (prop) {
                    var currentProp = stateObject[prop];

                    if (typeof currentProp === 'string' && currentProp.match(R_HEX)) {
                        stateObject[prop] = sanitizeHexChunksToRGB(currentProp);
                    }
                });
            }

            /*!
             * @param {string} str
             *
             * @return {string}
             */
            function  sanitizeHexChunksToRGB (str) {
                return filterStringChunks(R_HEX, str, convertHexToRGB);
            }

            /*!
             * @param {string} hexString
             *
             * @return {string}
             */
            function convertHexToRGB (hexString) {
                var rgbArr = hexToRGBArray(hexString);
                return 'rgb(' + rgbArr[0] + ',' + rgbArr[1] + ',' + rgbArr[2] + ')';
            }

            var hexToRGBArray_returnArray = [];
            /*!
             * Convert a hexadecimal string to an array with three items, one each for
             * the red, blue, and green decimal values.
             *
             * @param {string} hex A hexadecimal string.
             *
             * @returns {Array.<number>} The converted Array of RGB values if `hex` is a
             * valid string, or an Array of three 0's.
             */
            function hexToRGBArray (hex) {

                hex = hex.replace(/#/, '');

                // If the string is a shorthand three digit hex notation, normalize it to
                // the standard six digit notation
                if (hex.length === 3) {
                    hex = hex.split('');
                    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
                }

                hexToRGBArray_returnArray[0] = hexToDec(hex.substr(0, 2));
                hexToRGBArray_returnArray[1] = hexToDec(hex.substr(2, 2));
                hexToRGBArray_returnArray[2] = hexToDec(hex.substr(4, 2));

                return hexToRGBArray_returnArray;
            }

            /*!
             * Convert a base-16 number to base-10.
             *
             * @param {Number|String} hex The value to convert
             *
             * @returns {Number} The base-10 equivalent of `hex`.
             */
            function hexToDec (hex) {
                return parseInt(hex, 16);
            }

            /*!
             * Runs a filter operation on all chunks of a string that match a RegExp
             *
             * @param {RegExp} pattern
             * @param {string} unfilteredString
             * @param {function(string)} filter
             *
             * @return {string}
             */
            function filterStringChunks (pattern, unfilteredString, filter) {
                var pattenMatches = unfilteredString.match(pattern);
                var filteredString = unfilteredString.replace(pattern, VALUE_PLACEHOLDER);

                if (pattenMatches) {
                    var pattenMatchesLength = pattenMatches.length;
                    var currentChunk;

                    for (var i = 0; i < pattenMatchesLength; i++) {
                        currentChunk = pattenMatches.shift();
                        filteredString = filteredString.replace(
                            VALUE_PLACEHOLDER, filter(currentChunk));
                    }
                }

                return filteredString;
            }

            /*!
             * Check for floating point values within rgb strings and rounds them.
             *
             * @param {string} formattedString
             *
             * @return {string}
             */
            function sanitizeRGBChunks (formattedString) {
                return filterStringChunks(R_RGB, formattedString, sanitizeRGBChunk);
            }

            /*!
             * @param {string} rgbChunk
             *
             * @return {string}
             */
            function sanitizeRGBChunk (rgbChunk) {
                var numbers = rgbChunk.match(R_UNFORMATTED_VALUES);
                var numbersLength = numbers.length;
                var sanitizedString = rgbChunk.match(R_RGB_PREFIX)[0];

                for (var i = 0; i < numbersLength; i++) {
                    sanitizedString += parseInt(numbers[i], 10) + ',';
                }

                sanitizedString = sanitizedString.slice(0, -1) + ')';

                return sanitizedString;
            }

            /*!
             * @param {Object} stateObject
             *
             * @return {Object} An Object of formatManifests that correspond to
             * the string properties of stateObject
             */
            function getFormatManifests (stateObject) {
                var manifestAccumulator = {};

                Tweenable.each(stateObject, function (prop) {
                    var currentProp = stateObject[prop];

                    if (typeof currentProp === 'string') {
                        var rawValues = getValuesFrom(currentProp);

                        manifestAccumulator[prop] = {
                            'formatString': getFormatStringFrom(currentProp)
                            ,'chunkNames': getFormatChunksFrom(rawValues, prop)
                        };
                    }
                });

                return manifestAccumulator;
            }

            /*!
             * @param {Object} stateObject
             * @param {Object} formatManifests
             */
            function expandFormattedProperties (stateObject, formatManifests) {
                Tweenable.each(formatManifests, function (prop) {
                    var currentProp = stateObject[prop];
                    var rawValues = getValuesFrom(currentProp);
                    var rawValuesLength = rawValues.length;

                    for (var i = 0; i < rawValuesLength; i++) {
                        stateObject[formatManifests[prop].chunkNames[i]] = +rawValues[i];
                    }

                    delete stateObject[prop];
                });
            }

            /*!
             * @param {Object} stateObject
             * @param {Object} formatManifests
             */
            function collapseFormattedProperties (stateObject, formatManifests) {
                Tweenable.each(formatManifests, function (prop) {
                    var currentProp = stateObject[prop];
                    var formatChunks = extractPropertyChunks(
                        stateObject, formatManifests[prop].chunkNames);
                    var valuesList = getValuesList(
                        formatChunks, formatManifests[prop].chunkNames);
                    currentProp = getFormattedValues(
                        formatManifests[prop].formatString, valuesList);
                    stateObject[prop] = sanitizeRGBChunks(currentProp);
                });
            }

            /*!
             * @param {Object} stateObject
             * @param {Array.<string>} chunkNames
             *
             * @return {Object} The extracted value chunks.
             */
            function extractPropertyChunks (stateObject, chunkNames) {
                var extractedValues = {};
                var currentChunkName, chunkNamesLength = chunkNames.length;

                for (var i = 0; i < chunkNamesLength; i++) {
                    currentChunkName = chunkNames[i];
                    extractedValues[currentChunkName] = stateObject[currentChunkName];
                    delete stateObject[currentChunkName];
                }

                return extractedValues;
            }

            var getValuesList_accumulator = [];
            /*!
             * @param {Object} stateObject
             * @param {Array.<string>} chunkNames
             *
             * @return {Array.<number>}
             */
            function getValuesList (stateObject, chunkNames) {
                getValuesList_accumulator.length = 0;
                var chunkNamesLength = chunkNames.length;

                for (var i = 0; i < chunkNamesLength; i++) {
                    getValuesList_accumulator.push(stateObject[chunkNames[i]]);
                }

                return getValuesList_accumulator;
            }

            /*!
             * @param {string} formatString
             * @param {Array.<number>} rawValues
             *
             * @return {string}
             */
            function getFormattedValues (formatString, rawValues) {
                var formattedValueString = formatString;
                var rawValuesLength = rawValues.length;

                for (var i = 0; i < rawValuesLength; i++) {
                    formattedValueString = formattedValueString.replace(
                        VALUE_PLACEHOLDER, +rawValues[i].toFixed(4));
                }

                return formattedValueString;
            }

            /*!
             * Note: It's the duty of the caller to convert the Array elements of the
             * return value into numbers.  This is a performance optimization.
             *
             * @param {string} formattedString
             *
             * @return {Array.<string>|null}
             */
            function getValuesFrom (formattedString) {
                return formattedString.match(R_UNFORMATTED_VALUES);
            }

            /*!
             * @param {Object} easingObject
             * @param {Object} tokenData
             */
            function expandEasingObject (easingObject, tokenData) {
                Tweenable.each(tokenData, function (prop) {
                    var currentProp = tokenData[prop];
                    var chunkNames = currentProp.chunkNames;
                    var chunkLength = chunkNames.length;
                    var easingChunks = easingObject[prop].split(' ');
                    var lastEasingChunk = easingChunks[easingChunks.length - 1];

                    for (var i = 0; i < chunkLength; i++) {
                        easingObject[chunkNames[i]] = easingChunks[i] || lastEasingChunk;
                    }

                    delete easingObject[prop];
                });
            }

            /*!
             * @param {Object} easingObject
             * @param {Object} tokenData
             */
            function collapseEasingObject (easingObject, tokenData) {
                Tweenable.each(tokenData, function (prop) {
                    var currentProp = tokenData[prop];
                    var chunkNames = currentProp.chunkNames;
                    var chunkLength = chunkNames.length;
                    var composedEasingString = '';

                    for (var i = 0; i < chunkLength; i++) {
                        composedEasingString += ' ' + easingObject[chunkNames[i]];
                        delete easingObject[chunkNames[i]];
                    }

                    easingObject[prop] = composedEasingString.substr(1);
                });
            }

            Tweenable.prototype.filter.token = {
                'tweenCreated': function (currentState, fromState, toState, easingObject) {
                    sanitizeObjectForHexProps(currentState);
                    sanitizeObjectForHexProps(fromState);
                    sanitizeObjectForHexProps(toState);
                    this._tokenData = getFormatManifests(currentState);
                },

                'beforeTween': function (currentState, fromState, toState, easingObject) {
                    expandEasingObject(easingObject, this._tokenData);
                    expandFormattedProperties(currentState, this._tokenData);
                    expandFormattedProperties(fromState, this._tokenData);
                    expandFormattedProperties(toState, this._tokenData);
                },

                'afterTween': function (currentState, fromState, toState, easingObject) {
                    collapseFormattedProperties(currentState, this._tokenData);
                    collapseFormattedProperties(fromState, this._tokenData);
                    collapseFormattedProperties(toState, this._tokenData);
                    collapseEasingObject(easingObject, this._tokenData);
                }
            };

        } (Tweenable));

    }(this));

},{}],2:[function(require,module,exports){
// Circle shaped progress bar

    var Shape = require('./shape');
    var utils = require('./utils');


    var Circle = function Circle(container, options) {
        // Use two arcs to form a circle
        // See this answer http://stackoverflow.com/a/10477334/1446092
        this._pathTemplate =
            'M 50,50 m 0,-{radius}' +
            ' a {radius},{radius} 0 1 1 0,{2radius}' +
            ' a {radius},{radius} 0 1 1 0,-{2radius}';

        Shape.apply(this, arguments);
    };

    Circle.prototype = new Shape();
    Circle.prototype.constructor = Circle;

    Circle.prototype._pathString = function _pathString(opts) {
        var widthOfWider = opts.strokeWidth;
        if (opts.trailWidth && opts.trailWidth > opts.strokeWidth) {
            widthOfWider = opts.trailWidth;
        }

        var r = 50 - widthOfWider / 2;

        return utils.render(this._pathTemplate, {
            radius: r,
            '2radius': r * 2
        });
    };

    Circle.prototype._trailString = function _trailString(opts) {
        return this._pathString(opts);
    };

    module.exports = Circle;

},{"./shape":6,"./utils":8}],3:[function(require,module,exports){
// Line shaped progress bar

    var Shape = require('./shape');
    var utils = require('./utils');


    var Line = function Line(container, options) {
        this._pathTemplate = 'M 0,{center} L 100,{center}';
        Shape.apply(this, arguments);
    };

    Line.prototype = new Shape();
    Line.prototype.constructor = Line;

    Line.prototype._initializeSvg = function _initializeSvg(svg, opts) {
        svg.setAttribute('viewBox', '0 0 100 ' + opts.strokeWidth);
        svg.setAttribute('preserveAspectRatio', 'none');
    };

    Line.prototype._pathString = function _pathString(opts) {
        return utils.render(this._pathTemplate, {
            center: opts.strokeWidth / 2
        });
    };

    Line.prototype._trailString = function _trailString(opts) {
        return this._pathString(opts);
    };

    module.exports = Line;

},{"./shape":6,"./utils":8}],4:[function(require,module,exports){
// Different shaped progress bars
    var Line = require('./line');
    var Circle = require('./circle');
    var Square = require('./square');

// Lower level API to use any SVG path
    var Path = require('./path');


    module.exports = {
        Line: Line,
        Circle: Circle,
        Square: Square,
        Path: Path
    };

},{"./circle":2,"./line":3,"./path":5,"./square":7}],5:[function(require,module,exports){
// Lower level API to animate any kind of svg path

    var Tweenable = require('shifty');
    var utils = require('./utils');

    var EASING_ALIASES = {
        easeIn: 'easeInCubic',
        easeOut: 'easeOutCubic',
        easeInOut: 'easeInOutCubic'
    };


    var Path = function Path(path, opts) {
        // Default parameters for animation
        opts = utils.extend({
            duration: 800,
            easing: 'linear',
            from: {},
            to: {},
            step: function() {}
        }, opts);

        var element;
        if (utils.isString(path)) {
            element = document.querySelector(path);
        } else {
            element = path;
        }

        // Reveal .path as public attribute
        this.path = element;
        this._opts = opts;
        this._tweenable = null;

        // Set up the starting positions
        var length = this.path.getTotalLength();
        this.path.style.strokeDasharray = length + ' ' + length;
        this.set(0);
    };

    Path.prototype.value = function value() {
        var offset = this._getComputedDashOffset();
        var length = this.path.getTotalLength();

        var progress = 1 - offset / length;
        // Round number to prevent returning very small number like 1e-30, which
        // is practically 0
        return parseFloat(progress.toFixed(6), 10);
    };

    Path.prototype.set = function set(progress) {
        this.stop();

        this.path.style.strokeDashoffset = this._progressToOffset(progress);

        var step = this._opts.step;
        if (utils.isFunction(step)) {
            var easing = this._easing(this._opts.easing);
            var values = this._calculateTo(progress, easing);
            step(values, this._opts.shape||this, this._opts.attachment);
        }
    };

    Path.prototype.stop = function stop() {
        this._stopTween();
        this.path.style.strokeDashoffset = this._getComputedDashOffset();
    };

// Method introduced here:
// http://jakearchibald.com/2013/animated-line-drawing-svg/
    Path.prototype.animate = function animate(progress, opts, cb) {
        opts = opts || {};

        if (utils.isFunction(opts)) {
            cb = opts;
            opts = {};
        }

        var passedOpts = utils.extend({}, opts);

        // Copy default opts to new object so defaults are not modified
        var defaultOpts = utils.extend({}, this._opts);
        opts = utils.extend(defaultOpts, opts);

        var shiftyEasing = this._easing(opts.easing);
        var values = this._resolveFromAndTo(progress, shiftyEasing, passedOpts);

        this.stop();

        // Trigger a layout so styles are calculated & the browser
        // picks up the starting position before animating
        this.path.getBoundingClientRect();

        var offset = this._getComputedDashOffset();
        var newOffset = this._progressToOffset(progress);

        var self = this;
        this._tweenable = new Tweenable();
        this._tweenable.tween({
            from: utils.extend({ offset: offset }, values.from),
            to: utils.extend({ offset: newOffset }, values.to),
            duration: opts.duration,
            easing: shiftyEasing,
            step: function(state) {
                self.path.style.strokeDashoffset = state.offset;
                opts.step(state, opts.shape||self, opts.attachment);
            },
            finish: function(state) {
                if (utils.isFunction(cb)) {
                    cb();
                }
            }
        });
    };

    Path.prototype._getComputedDashOffset = function _getComputedDashOffset() {
        var computedStyle = window.getComputedStyle(this.path, null);
        return parseFloat(computedStyle.getPropertyValue('stroke-dashoffset'), 10);
    };

    Path.prototype._progressToOffset = function _progressToOffset(progress) {
        var length = this.path.getTotalLength();
        return length - progress * length;
    };

// Resolves from and to values for animation.
    Path.prototype._resolveFromAndTo = function _resolveFromAndTo(progress, easing, opts) {
        if (opts.from && opts.to) {
            return {
                from: opts.from,
                to: opts.to
            };
        }

        return {
            from: this._calculateFrom(easing),
            to: this._calculateTo(progress, easing)
        };
    };

// Calculate `from` values from options passed at initialization
    Path.prototype._calculateFrom = function _calculateFrom(easing) {
        return Tweenable.interpolate(this._opts.from, this._opts.to, this.value(), easing);
    };

// Calculate `to` values from options passed at initialization
    Path.prototype._calculateTo = function _calculateTo(progress, easing) {
        return Tweenable.interpolate(this._opts.from, this._opts.to, progress, easing);
    };

    Path.prototype._stopTween = function _stopTween() {
        if (this._tweenable !== null) {
            this._tweenable.stop();
            this._tweenable.dispose();
            this._tweenable = null;
        }
    };

    Path.prototype._easing = function _easing(easing) {
        if (EASING_ALIASES.hasOwnProperty(easing)) {
            return EASING_ALIASES[easing];
        }

        return easing;
    };

    module.exports = Path;

},{"./utils":8,"shifty":1}],6:[function(require,module,exports){
// Base object for different progress bar shapes

    var Path = require('./path');
    var utils = require('./utils');

    var DESTROYED_ERROR = 'Object is destroyed';


    var Shape = function Shape(container, opts) {
        // Throw a better error if progress bars are not initialized with `new`
        // keyword
        if (!(this instanceof Shape)) {
            throw new Error('Constructor was called without new keyword');
        }

        // Prevent calling constructor without parameters so inheritance
        // works correctly. To understand, this is how Shape is inherited:
        //
        //   Line.prototype = new Shape();
        //
        // We just want to set the prototype for Line.
        if (arguments.length === 0) return;

        // Default parameters for progress bar creation
        this._opts = utils.extend({
            color: '#555',
            strokeWidth: 1.0,
            trailColor: null,
            trailWidth: null,
            fill: null,
            text: {
                autoStyle: true,
                color: null,
                value: '',
                className: 'progressbar-text'
            }
        }, opts, true);  // Use recursive extend

        var svgView = this._createSvgView(this._opts);

        var element;
        if (utils.isString(container)) {
            element = document.querySelector(container);
        } else {
            element = container;
        }

        if (!element) {
            throw new Error('Container does not exist: ' + container);
        }

        this._container = element;
        this._container.appendChild(svgView.svg);

        this.text = null;
        if (this._opts.text.value) {
            this.text = this._createTextElement(this._opts, this._container);
            this._container.appendChild(this.text);
        }

        // Expose public attributes before Path initialization
        this.svg = svgView.svg;
        this.path = svgView.path;
        this.trail = svgView.trail;
        // this.text is also a public attribute

        var newOpts = utils.extend({
            attachment: undefined,
            shape: this
        }, this._opts);
        this._progressPath = new Path(svgView.path, newOpts);
    };

    Shape.prototype.animate = function animate(progress, opts, cb) {
        if (this._progressPath === null) throw new Error(DESTROYED_ERROR);
        this._progressPath.animate(progress, opts, cb);
    };

    Shape.prototype.stop = function stop() {
        if (this._progressPath === null) throw new Error(DESTROYED_ERROR);
        // Don't crash if stop is called inside step function
        if (this._progressPath === undefined) return;

        this._progressPath.stop();
    };

    Shape.prototype.destroy = function destroy() {
        if (this._progressPath === null) throw new Error(DESTROYED_ERROR);

        this.stop();
        this.svg.parentNode.removeChild(this.svg);
        this.svg = null;
        this.path = null;
        this.trail = null;
        this._progressPath = null;

        if (this.text !== null) {
            this.text.parentNode.removeChild(this.text);
            this.text = null;
        }
    };

    Shape.prototype.set = function set(progress) {
        if (this._progressPath === null) throw new Error(DESTROYED_ERROR);
        this._progressPath.set(progress);
    };

    Shape.prototype.value = function value() {
        if (this._progressPath === null) throw new Error(DESTROYED_ERROR);
        if (this._progressPath === undefined) return 0;

        return this._progressPath.value();
    };

    Shape.prototype.setText = function setText(text) {
        if (this._progressPath === null) throw new Error(DESTROYED_ERROR);

        if (this.text === null) {
            // Create new text node
            this.text = this._createTextElement(this._opts, this._container);
            this._container.appendChild(this.text);
        }

        // Remove previous text node and add new
        this.text.removeChild(this.text.firstChild);
        this.text.appendChild(document.createTextNode(text));
    };

    Shape.prototype._createSvgView = function _createSvgView(opts) {
        var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this._initializeSvg(svg, opts);

        var trailPath = null;
        // Each option listed in the if condition are 'triggers' for creating
        // the trail path
        if (opts.trailColor || opts.trailWidth) {
            trailPath = this._createTrail(opts);
            svg.appendChild(trailPath);
        }

        var path = this._createPath(opts);
        svg.appendChild(path);

        return {
            svg: svg,
            path: path,
            trail: trailPath
        };
    };

    Shape.prototype._initializeSvg = function _initializeSvg(svg, opts) {
        svg.setAttribute('viewBox', '0 0 100 100');
    };

    Shape.prototype._createPath = function _createPath(opts) {
        var pathString = this._pathString(opts);
        return this._createPathElement(pathString, opts);
    };

    Shape.prototype._createTrail = function _createTrail(opts) {
        // Create path string with original passed options
        var pathString = this._trailString(opts);

        // Prevent modifying original
        var newOpts = utils.extend({}, opts);

        // Defaults for parameters which modify trail path
        if (!newOpts.trailColor) newOpts.trailColor = '#eee';
        if (!newOpts.trailWidth) newOpts.trailWidth = newOpts.strokeWidth;

        newOpts.color = newOpts.trailColor;
        newOpts.strokeWidth = newOpts.trailWidth;

        // When trail path is set, fill must be set for it instead of the
        // actual path to prevent trail stroke from clipping
        newOpts.fill = null;

        return this._createPathElement(pathString, newOpts);
    };

    Shape.prototype._createPathElement = function _createPathElement(pathString, opts) {
        var path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path.setAttribute('d', pathString);
        path.setAttribute('stroke', opts.color);
        path.setAttribute('stroke-width', opts.strokeWidth);

        if (opts.fill) {
            path.setAttribute('fill', opts.fill);
        } else {
            path.setAttribute('fill-opacity', '0');
        }

        return path;
    };

    Shape.prototype._createTextElement = function _createTextElement(opts, container) {
        var element = document.createElement('p');
        element.appendChild(document.createTextNode(opts.text.value));

        if (opts.text.autoStyle) {
            // Center text
            container.style.position = 'relative';
            element.style.position = 'absolute';
            element.style.top = '50%';
            element.style.left = '50%';
            element.style.padding = 0;
            element.style.margin = 0;
            utils.setStyle(element, 'transform', 'translate(-50%, -50%)');

            if (opts.text.color) {
                element.style.color = opts.text.color;
            } else {
                element.style.color = opts.color;
            }
        }
        element.className = opts.text.className;

        return element;
    };

    Shape.prototype._pathString = function _pathString(opts) {
        throw new Error('Override this function for each progress bar');
    };

    Shape.prototype._trailString = function _trailString(opts) {
        throw new Error('Override this function for each progress bar');
    };

    module.exports = Shape;

},{"./path":5,"./utils":8}],7:[function(require,module,exports){
// Square shaped progress bar

    var Shape = require('./shape');
    var utils = require('./utils');


    var Square = function Square(container, options) {
        this._pathTemplate =
            'M 0,{halfOfStrokeWidth}' +
            ' L {width},{halfOfStrokeWidth}' +
            ' L {width},{width}' +
            ' L {halfOfStrokeWidth},{width}' +
            ' L {halfOfStrokeWidth},{strokeWidth}';

        this._trailTemplate =
            'M {startMargin},{halfOfStrokeWidth}' +
            ' L {width},{halfOfStrokeWidth}' +
            ' L {width},{width}' +
            ' L {halfOfStrokeWidth},{width}' +
            ' L {halfOfStrokeWidth},{halfOfStrokeWidth}';

        Shape.apply(this, arguments);
    };

    Square.prototype = new Shape();
    Square.prototype.constructor = Square;

    Square.prototype._pathString = function _pathString(opts) {
        var w = 100 - opts.strokeWidth / 2;

        return utils.render(this._pathTemplate, {
            width: w,
            strokeWidth: opts.strokeWidth,
            halfOfStrokeWidth: opts.strokeWidth / 2
        });
    };

    Square.prototype._trailString = function _trailString(opts) {
        var w = 100 - opts.strokeWidth / 2;

        return utils.render(this._trailTemplate, {
            width: w,
            strokeWidth: opts.strokeWidth,
            halfOfStrokeWidth: opts.strokeWidth / 2,
            startMargin: (opts.strokeWidth / 2) - (opts.trailWidth / 2)
        });
    };

    module.exports = Square;

},{"./shape":6,"./utils":8}],8:[function(require,module,exports){
// Utility functions

    var PREFIXES = 'Webkit Moz O ms'.split(' ');

// Copy all attributes from source object to destination object.
// destination object is mutated.
    function extend(destination, source, recursive) {
        destination = destination || {};
        source = source || {};
        recursive = recursive || false;

        for (var attrName in source) {
            if (source.hasOwnProperty(attrName)) {
                var destVal = destination[attrName];
                var sourceVal = source[attrName];
                if (recursive && isObject(destVal) && isObject(sourceVal)) {
                    destination[attrName] = extend(destVal, sourceVal, recursive);
                } else {
                    destination[attrName] = sourceVal;
                }
            }
        }

        return destination;
    }

// Renders templates with given variables. Variables must be surrounded with
// braces without any spaces, e.g. {variable}
// All instances of variable placeholders will be replaced with given content
// Example:
// render('Hello, {message}!', {message: 'world'})
    function render(template, vars) {
        var rendered = template;

        for (var key in vars) {
            if (vars.hasOwnProperty(key)) {
                var val = vars[key];
                var regExpString = '\\{' + key + '\\}';
                var regExp = new RegExp(regExpString, 'g');

                rendered = rendered.replace(regExp, val);
            }
        }

        return rendered;
    }

    function setStyle(element, style, value) {
        for (var i = 0; i < PREFIXES.length; ++i) {
            var prefix = PREFIXES[i];
            element.style[prefix + capitalize(style)] = value;
        }

        element.style[style] = value;
    }

    function capitalize(text) {
        return text.charAt(0).toUpperCase() + text.slice(1);
    }

    function isString(obj) {
        return typeof obj === 'string' || obj instanceof String;
    }

    function isFunction(obj) {
        return typeof obj === 'function';
    }

    function isArray(obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }

// Returns true if `obj` is object as in {a: 1, b: 2}, not if it's function or
// array
    function isObject(obj) {
        if (isArray(obj)) return false;

        var type = typeof obj;
        return type === 'object' && !!obj;
    }


    module.exports = {
        extend: extend,
        render: render,
        setStyle: setStyle,
        capitalize: capitalize,
        isString: isString,
        isFunction: isFunction,
        isObject: isObject
    };

},{}]},{},[4])(4)
});