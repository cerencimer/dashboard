<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.8.16/themes/base/jquery-ui.css"/>
		<link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.8.16/themes/base/jquery.ui.theme.css"/>
		<script src="jquery/jquery.js"></script>
		<script src="js/progressbar.js"></script>
		<script src="js/Chart/Chart.js"></script>
		<script src="js/myChart.js"></script>
		<script src="js/Chart/combinationChart.js"></script>
		<script src="js/main.js"></script>
		<script type='text/javascript' src='http://code.jquery.com/jquery-1.7.1.js'></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.js"></script>
		<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css">

		<script src="http://code.highcharts.com/highcharts.js"></script>
		<script src="http://code.highcharts.com/modules/exporting.js"></script>

		<style>
			body,html{
				padding: 0;
				margin: 0;
				}
			div
			{
				font-size:8pt;
				font-family:Verdana;
				padding: 5px;
			}
			.wrapper{
				height: auto;
				width: 100%;
				}
			#container{
				min-width: 400px;
				width: 100%;
				height: 550px;
				margin: 0 auto;
				display: none;
				}
			#example-percent-container{
				max-width: 250px;
				height: 250px;
				margin: 0 auto;

			}
			#div1{

				color:#777777;
			}


			.myButton {
				 margin:auto;
				-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
				-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
				box-shadow:inset 0px 1px 0px 0px #ffffff;
				background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf));
				background:-moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
				background:-webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
				background:-o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
				background:-ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
				background:linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
				filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf',GradientType=0);
				background-color:#ededed;
				-moz-border-radius:6px;
				-webkit-border-radius:6px;
				border-radius:6px;
				border:1px solid #dcdcdc;
				display:inline-block;
				cursor:pointer;
				color:#777777;
				font-family:Arial;
				font-size:15px;
				font-weight:bold;
				padding:6px 24px;
				text-decoration:none;
				text-shadow:0px 1px 0px #ffffff;
			}
			.myButton:hover {
				background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed));
				background:-moz-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
				background:-webkit-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
				background:-o-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
				background:-ms-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
				background:linear-gradient(to bottom, #dfdfdf 5%, #ededed 100%);
				filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed',GradientType=0);
				background-color:#dfdfdf;
			}
			.myButton:active {
				position:relative;
				top:1px;
			}




		</style>
	</head>
	<body bgcolor="#3e3e40">
	<div class="wrapper">
		<div id="container"></div>

	<div id="example-percent-container"></div>

		<div id="div1">
			<table id="dateTable" style="width:100% border: 2px ; margin-left:110px  ">
				<tr>
					<td><label style="color: beige; font-size:medium">From:</label></td>
					<td> <input type="text" id="txtFromDate" style="background-color: #cceecc; font-size:medium" width="170px" height="80px" /></td>
				</tr>
				<tr>
					<td><label style="color: beige; font-size:medium">To  :</label></td>
					<td><input type="text" id="txtToDate" width="170px" style="background-color: #cceecc; font-size:medium"  height="80px"/></td> </tr>
				</tr>
				<tr>
					<td><label></label></td>
					<td><button class="myButton">push me!</button></td>
				</tr>
			</table></div>

		</div>

	</div>

	</body>
</html>
