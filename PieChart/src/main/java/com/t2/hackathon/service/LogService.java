package com.t2.hackathon.service;
import com.t2.hackathon.dao.LogReader;
import com.t2.hackathon.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
@Service
public class LogService {


    @Autowired
    private LogReader logReader;

    public ReturnType getLogs(String startDate, String endDate) throws Exception{


        List<Islem> islemler = logReader.getData(startDate, endDate);
//        List<Islem> islemOnMemory = new ArrayList<>();
//
//        Islem gecIslem = new Islem();
//        gecIslem.setIslemSayisi("100");
//        gecIslem.setSegment("ACC-001");
//        gecIslem.setIslemTipi("Musteri");
//        gecIslem.setIslemAciklama("Musteri");
//        islemOnMemory.add(gecIslem);
//
//        gecIslem = new Islem();
//        gecIslem.setIslemSayisi("25");
//        gecIslem.setSegment("ACC-002");
//        gecIslem.setIslemTipi("Musteri");
//        gecIslem.setIslemAciklama("Musteri");
//        islemOnMemory.add(gecIslem);
//
//        gecIslem = new Islem();
//        gecIslem.setIslemSayisi("5");
//        gecIslem.setSegment("ACC-001");
//        gecIslem.setIslemTipi("Abone");
//        gecIslem.setIslemAciklama("Abone");
//        islemOnMemory.add(gecIslem);
//
//        gecIslem = new Islem();
//        gecIslem.setIslemSayisi("15");
//        gecIslem.setSegment("ACC-002");
//        gecIslem.setIslemTipi("Abone");
//        gecIslem.setIslemAciklama("Abone");
//        islemOnMemory.add(gecIslem);

        Result result = new Result();

        for (Islem islem : islemler){//islemOnMemory){ /////islemOnMemory---->islemler

            result.addCategory(islem.getIslemAciklama());
            result.addColumnbar(islem);
            result.addPieChart( islem);
        }

        result.getPieChart();

        List<Pie> topTenOperations = result.getPieChart().getData();

        Collections.sort(topTenOperations, new Comparator<Pie>() {
            public int compare(Pie o1, Pie o2) {
                return o2.getY() - o1.getY();
            }
        });

        if(result.getPieChart().getData().size()>10 ) {
            do{
                result.getPieChart().getData().remove(result.getPieChart().getData().size() - 1);
            }while(result.getPieChart().getData().size()==10);
        }

        if(topTenOperations.size()>10 ) {
            do{
                topTenOperations.remove(topTenOperations.size() - 1);
            }while(topTenOperations.size()>10);
        }


        result.adjustCategories(topTenOperations);
        result.adjustColumnBarData(topTenOperations);

        ReturnType returnType = new ReturnType();
        returnType.setReturnData(result);
        return returnType;
    }


}
