package com.t2.hackathon.service;

import com.t2.hackathon.dao.LogReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Clanses on 31.7.2015.
 */

@Service
public class CronJob  {

    @Autowired
    private LogReader logReader;
    private  Date jobDate;
    private static  int counter = 0;
    public Date getJobDate() {
        return jobDate;
    }

    public void setJobDate(Date jobDate) {
        this.jobDate = jobDate;
    }


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");


    @Scheduled(cron = "0 0/5 * * * ?")

    public void reportCurrentTime() throws Exception {

      if(counter%2==0) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MONTH, -3);
    Date now = cal.getTime();
    setJobDate(now);
    counter++;
    System.out.println("The time is now " + dateFormat.format(now));
    logReader.getJobsDate(now);
        }
       else
       counter++;


    }

}
