package com.t2.hackathon.controller;

import com.t2.hackathon.model.ReturnType;
import com.t2.hackathon.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class GetLogController {

        @Autowired
        private LogService logService;


        @RequestMapping(value = "/getLastFiveMinutes", method = RequestMethod.POST)
        public @ResponseBody
        ReturnType getLastFiveMinutes( @RequestParam String startDate, @RequestParam String endDate ) throws Exception{
                try{
                        return logService.getLogs(startDate, endDate);
                }catch(Exception ex){
                        ex.printStackTrace();
                        return null;
                }

        }


}
