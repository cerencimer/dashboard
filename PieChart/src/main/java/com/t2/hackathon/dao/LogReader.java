package com.t2.hackathon.dao;

import com.t2.hackathon.model.Islem;
import com.t2.hackathon.model.WebServerLog;
import com.t2.hackathon.service.CronJob;
import com.t2.hackathon.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.*;


@Repository
public class LogReader {

//
    /** The jdbc template. */
    @Autowired
    @Qualifier("jdbcTemplate")
    protected JdbcTemplate jdbcTemplate;

    /** The named parameter jdbc template. */
    @Autowired
    @Qualifier("namedParameterJdbcTemplate")
    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private SimpleJdbcInsert insertCronJob;

    @PostConstruct
    public void afterPropertiesSet() {
        this.insertCronJob = new SimpleJdbcInsert(jdbcTemplate).withTableName("CRONJOB");
    }


    private RowMapper<WebServerLog> rowMapperError = new RowMapper<WebServerLog>() {
        @Override
        public WebServerLog mapRow(ResultSet rs, int rowNum) throws SQLException {

            WebServerLog islem = new WebServerLog();
            islem.setUUID(rs.getString("UUID"));
            islem.setIstek_xml(rs.getString("ISTEK_XML"));
            islem.setCevap_xml(rs.getString("CEVAP_XML"));
            islem.setBaslangicTarihi(rs.getString("ISLEM_BASLANGIC"));
            islem.setBitisTarihi(rs.getString("ISLEM_BITIS"));
            islem.setIslemTipi(rs.getString("ISLEM_TIPI"));

            return islem;
        }
    };
    private RowMapper<CronJob> rowMapperJob = new RowMapper<CronJob>() {
        @Override
        public CronJob mapRow(ResultSet rs, int rowNum) throws SQLException {

            CronJob job = new CronJob();
            job.setJobDate(rs.getDate("JOB_DATE"));
            return job;
        }
    };

    public String getLastDate() throws Exception{
        Map<String, Object> parameters = new HashMap<String, Object>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String sqlQuery1="SELECT * FROM (SELECT JOB_DATE FROM CRONJOB  ORDER BY  CRONJOB.JOB_DATE ASC) WHERE ROWNUM<=1";

        List<CronJob> liste = namedParameterJdbcTemplate.query(sqlQuery1, parameters, rowMapperJob);
        Date start= liste.get(0).getJobDate();
        String startDate=dateFormat.format(start);
        return startDate;



    }

    public List <WebServerLog> getErrorsBetweenDate(String startDate,String endDate) throws Exception{
        Map<String, Object>  errors = new HashMap<String, Object>();

//        String sqlQuery="SELECT * FROM OHM_WS_LOG WHERE CEVAP_XML IS NULL AND ISLEM_BASLANGIC BETWEEN TO_DATE('"+startDate+"','DD-MM-YY HH24:MI') AND TO_DATE('"+endDate+"','DD-MM-YY HH24:MI')";
        String sqlQuery2="SELECT * FROM OHM_WS_LOG WHERE CEVAP_XML IS NULL AND ISLEM_BASLANGIC BETWEEN TO_DATE('03-04-2015','DD-MM-YY HH24:MI') AND TO_DATE('03-05-2015','DD-MM-YY HH24:MI')";
        System.out.println(sqlQuery2);
        List<WebServerLog> liste = namedParameterJdbcTemplate.query(sqlQuery2,errors,rowMapperError);


        return liste;


    }

    public void getJobsDate(Date jobdate) throws  Exception{

        String sqlQuery ="INSERT INTO CRONJOB (JOB_DATE) values (TO_DATE('"+jobdate+"', 'DD-MM-YY HH24:MI'));";
        System.out.println(sqlQuery);

        MapSqlParameterSource jobParameters = new MapSqlParameterSource()
                .addValue("JOB_DATE", jobdate, Types.TIMESTAMP);

        insertCronJob.execute(jobParameters);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -3);
        Date now = cal.getTime();
        String endDate= dateFormat.format(now);
        String startDate=getLastDate();
        List<WebServerLog> list= new ArrayList<WebServerLog>(getErrorsBetweenDate(startDate,endDate));
        EmailService email = new EmailService();
        email.sendEmail(list);
        System.out.println("Null Errors is done.");

    }


    private RowMapper<Islem> rowMapper = new RowMapper<Islem>() {
        @Override
        public Islem mapRow(ResultSet rs, int rowNum) throws SQLException {

            Islem islem = new Islem();
            islem.setIslemTipi(rs.getString("ISLEM_TIPI"));
            islem.setIslemSayisi(rs.getString("ISLEM_SAYISI"));
            islem.setIslemAciklama(rs.getString("KISA_ACIKLAMA"));
            islem.setSegment(rs.getString("REGULAR_MARKET_SEGMENT"));
            return islem;
        }
    };

    public List<Islem> getData(String startDate, String endDate) throws Exception{

        Map<String, Object> parameters = new HashMap<String, Object>();
        String sqlQuery = "SELECT COUNT(*) ISLEM_SAYISI,OHM_OPERATION_LOG.ISLEM_TIPI ,OHM_LOG_ISLEM_TIPI.KISA_ACIKLAMA ,TMS.MUSTERI.REGULAR_MARKET_SEGMENT FROM OHM_OPERATION_LOG INNER JOIN OHM_LOG ON OHM_OPERATION_LOG.UUID = OHM_LOG.UUID INNER JOIN OHM_LOG_ISLEM_TIPI ON OHM_OPERATION_LOG.ISLEM_TIPI=OHM_LOG_ISLEM_TIPI.ID INNER JOIN K_SS_KULLANICI ON OHM_LOG.KULLANICI_KODU= K_SS_KULLANICI.KULLANICI_KODU INNER JOIN TMS.MUSTERI ON K_SS_KULLANICI.CRM_MUSTERI_ID=TMS.MUSTERI.CRM_CUSTOMER_ID WHERE  OHM_OPERATION_LOG.ISLEM_BASLANGIC between TO_DATE('"+startDate+" 00:00','DD-MM-YY HH24:MI') and TO_DATE('"+endDate+" 00:00','DD-MM-YY HH24:MI') GROUP BY OHM_OPERATION_LOG.ISLEM_TIPI, TMS.MUSTERI.REGULAR_MARKET_SEGMENT ,OHM_LOG_ISLEM_TIPI.KISA_ACIKLAMA";
        System.out.println(sqlQuery);

        List<Islem> liste = namedParameterJdbcTemplate.query(sqlQuery, parameters, rowMapper);
        System.out.println("First mission is completed.");
          return liste;

    }


}
