package com.t2.hackathon.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clanses on 22.7.2015.
 */
public class PieChart {
    private final String type = "pie";
    private String name="Pie Chart";
    private List<Pie> data;
    private int size;
    private boolean showInLegend;
    private int[] center;

   private Enabled dataLabels;

    public PieChart() {
        data = new ArrayList<>();
        dataLabels = new Enabled();
        center = new int[2];
        center[0]=1000;
        center[1]=100;
        size = 220;
        showInLegend= false;
    }

    public int[] getCenter() {
        return center;
    }

    public void setCenter(int[] center) {
        this.center = center;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isShowInLegend() {
        return showInLegend;
   }

    public void setShowInLegend(boolean showInLegend) {
        this.showInLegend = showInLegend;
    }

    public String getType() {
        return type;
    }

    public String getName() {

        return name;
    }

    public List<Pie> getData() {

        return this.data;
    }

    public Enabled getDataLabels() {
        return dataLabels;
    }

    public void setDataLabels(Enabled dataLabels) {
        this.dataLabels = dataLabels;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setPies(List<Pie> data) {

        this.data = data;
    }

    public void updateOrInsertPie(Islem islem) {

        for (Pie pie : data) {
            if (pie.getName().equals(islem.getIslemAciklama())) {
                pie.setY(pie.getY() + Integer.decode(islem.getIslemSayisi()));
                return;
            }
        }

        Pie pie1 = new Pie();
        pie1.setName(islem.getIslemAciklama());
        pie1.setY(Integer.decode(islem.getIslemSayisi()));
        data.add(pie1);
    }
}
