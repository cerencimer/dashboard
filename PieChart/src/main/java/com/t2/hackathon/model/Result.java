package com.t2.hackathon.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Clanses on 21.7.2015.
 */
public class Result {
    public List<String> categories;
    public List<ColumnBar> columnBarList;
    public PieChart pieChart;

    public Result() {
        categories = new ArrayList<>();
        columnBarList = new ArrayList<>();
        pieChart = new PieChart();

    }

    public void addCategory(String categoryName){
        if (!categories.contains(categoryName)){
            categories.add(categoryName);
        }
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<ColumnBar> getColumnBarList() {
        return columnBarList;
    }

    public PieChart getPieChart() {
        return pieChart;
    }

    public void setPieChart(PieChart pieChart) {
        this.pieChart = pieChart;
    }
//    public List<PieChart> getPieCharts() {
//        return pieCharts;
//    }
//
//    public void setPieCharts(List<PieChart> pieCharts) {
//        this.pieCharts = pieCharts;
//    }

    public void setColumnBarList(List<ColumnBar> columnBarList) {
        this.columnBarList = columnBarList;
    }


    public void addPieChart(Islem islem){
        if (pieChart.getData().size() > 0){

           // PieChart pieChart = pieChartList.get(0);
            pieChart.updateOrInsertPie(islem);
        }
        else{
           // PieChart pieChart = new PieChart();
            Pie pie = new Pie();
            pie.setName(islem.getIslemAciklama());
            pie.setY(Integer.decode(islem.getIslemSayisi()));
            pieChart.getData().add(pie);
           // pieChartList.add(pieChart);
        }
    }


    public void addColumnbar(Islem islem){
        ColumnBar columnBar = contains(islem);
        if (columnBar.hasValue()){
            columnBar.addOperationTypeSegmentCount(islem);
        }
        else{
            columnBar.setName(islem.getSegment());
            columnBar.addOperationTypeSegmentCount(islem);
            columnBarList.add(columnBar);
        }
    }

    public void adjustCategories(List<Pie> topTenOperations){

         List<String> arrangedCategories =new ArrayList<String>();

        for( Pie pie: topTenOperations){

            arrangedCategories.add(pie.getName());
        }
        setCategories(arrangedCategories);
    }

    public void adjustColumnBarData(List<Pie> topTenOperations){

         for(ColumnBar clmb : columnBarList){

             LinkedHashMap<String, Integer> arrangedValues = new LinkedHashMap<>();

             for( Pie pie: topTenOperations){

                 if( clmb.giveValues().get(pie.getName())==null ){

                     String name = pie.getName();
                     arrangedValues.put(name,0);
                 }
                 else{

                     int value = clmb.giveValues().get(pie.getName());
                     arrangedValues.put(pie.getName(), value);
                 }
             }

            //clmb.values = arrangedValues;
             clmb.setValues(arrangedValues);
         }

    }


    public void adjust(){
        for (String category : categories){
            for (ColumnBar columnBar: columnBarList){
                columnBar.adjustOperationTypesCount(category);
            }
        }
    }


    private ColumnBar contains(Islem islem) {
        for (ColumnBar columnBar : columnBarList){
            if (columnBar.getName().equals(islem.getSegment())){
                return columnBar;
            }
        }
        return new ColumnBar();
    }
}
