package com.t2.hackathon.model;

import java.util.List;

/**
 * Created by Clanses on 02.07.2015.
 */
public class Islem {

    public String islemTipi;
    public String islemSayisi;
    public String islemAciklama;
    public String segment;

    public  Islem() {

    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getIslemAciklama() {
        return islemAciklama;
    }

    public void setIslemAciklama(String islemAciklama) {
        this.islemAciklama = islemAciklama;
    }

    public String getIslemSayisi() {
        return islemSayisi;
    }
    public void setIslemSayisi(String islemSayisi) {
        this.islemSayisi = islemSayisi;
    }
    public String getIslemTipi() {
        return islemTipi;
    }
    public void setIslemTipi(String islemTipi) {
        this.islemTipi = islemTipi;
    }


}

