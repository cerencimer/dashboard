package com.t2.hackathon.model;

/**
 * Created by Clanses on 29.7.2015.
 */
public class Enabled {

    private Boolean enabled;

    public Enabled() {
        this.enabled = false;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

}
