package com.t2.hackathon.model;

/**
 * Created by Clanses on 3.8.2015.
 */
public class WebServerLog {

    public String UUID = new String();
    public String islemTipi = new String();
    public String  istek_xml = new String();
    public String cevap_xml = new String();
    public String bitisTarihi = new String();
    public String baslangicTarihi= new String();


    public void WebServerLog()
    {

    }

    public String getBaslangicTarihi() {
        return baslangicTarihi;
    }

    public void setBaslangicTarihi(String baslangicTarihi) {
        this.baslangicTarihi = baslangicTarihi;
    }

    public String getBitisTarihi() {
        return bitisTarihi;
    }

    public void setBitisTarihi(String bitisTarihi) {
        this.bitisTarihi = bitisTarihi;
    }



    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }



    public String getIstek_xml() {
        return istek_xml;
    }

    public void setIstek_xml(String istek_xml) {
        this.istek_xml = istek_xml;
    }



    public String getCevap_xml() {
        return cevap_xml;
    }

    public void setCevap_xml(String cevap_xml) {
        this.cevap_xml = cevap_xml;
    }



    public String getIslemTipi() {
        return islemTipi;
    }

    public void setIslemTipi(String islemTipi) {
        this.islemTipi = islemTipi;
    }




}
