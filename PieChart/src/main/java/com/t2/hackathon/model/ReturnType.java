package com.t2.hackathon.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.io.Serializable;

/**
 * Created by Clanses on 29/06/15.
 */
@JsonAutoDetect
public class ReturnType implements Serializable {
    private static final long serialVersionUID = -6287289049672546287L;

    private Object returnData;

    public Object getReturnData() {
        return returnData;
    }

    public void setReturnData(Object returnData) {
        this.returnData = returnData;
    }
}