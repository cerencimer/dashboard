package com.t2.hackathon.model;

import java.util.HashMap;

/**
 * Created by Clanses on 22.7.2015.
 */
public class ColumnBar {
    private final String type = "column";
    private String name;
    public HashMap <String, Integer> values;

    public ColumnBar() {
        values = new HashMap<>();
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String segmentName) {
        this.name = segmentName;
    }

    public int[] getData() {
        int[] resultData = new int[values.size()];
        int start = 0;
        for (String key : values.keySet()){
            resultData[start++] = values.get(key);
        }
        return resultData;
    }


    public HashMap<String, Integer> giveValues() {
        return values;
    }

    public void setValues(HashMap<String, Integer> values) {

        this.values = values;
    }

    public boolean hasValue(){
        return values.size() > 0;
    }

    public void adjustOperationTypesCount(String operationType){
        if (!values.containsKey(operationType)){
            values.put(operationType, 0);
        }
    }

    public void addOperationTypeSegmentCount(Islem islem){
       // values.put(islem.getIslemTipi(), Integer.parseInt(islem.getIslemSayisi()));

        values.put(islem.getIslemAciklama(), Integer.parseInt(islem.getIslemSayisi()));

    }
}
